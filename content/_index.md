+++
title = 'support.laredoute.io'
date = 2024-04-06T14:41:52+01:00
draft = false
+++

Welcome to the IT Support Portal!

Should you have any request or incident to submit to IT staff, Feel free to use one of these portals.
On the left you can find further information in the respective page

---

#### Operations links

>- [OPS-DBA Jira Portal](https://ops-jira-portal)
>- [OPS-Network Jira Portal](https://ops-jira-portal)
>- [OPS-Infra Jira Portal](https://ops-jira-portal)

#### IT Support links

>- [IT Link 1](https://ops-jira-portal)
>- [IT Link 2](https://ops-jira-portal)

#### Security Support links

>- [Sec Link 1](https://ops-jira-portal)
>- [Sec Link 2](https://ops-jira-portal)

#### Application Support links

>- [Application Support link 1](https://ops-jira-portal)
>- [Application Support link 2](https://ops-jira-portal)

