+++
title = 'Operations'
date = 2024-04-06T13:59:56+01:00
draft = false
+++

### Operations page

Here are our Confluence Documentation pages:
>- [OPS Infra](https://laredoute.atlassian.net/wiki/spaces/OPSINFRA/overview)
>- [OPS Pros](https://laredoute.atlassian.net/wiki/spaces/II/overview)
>- [OPS DBA](https://laredoute.atlassian.net/wiki/spaces/OPSDBA/overview)
>- [OPS Network](https://laredoute.atlassian.net/wiki/spaces/NETWORK/overview)

Here are our Jira Projects:
>- [ITOPS](https://laredoute.atlassian.net/jira/core/projects/ITOPS/board)
>- [OPS](https://laredoute.atlassian.net/jira/software/c/projects/OPS/boards/280)
>- [DBA](https://laredoute.atlassian.net/jira/software/c/projects/OPSDBA/boards/483)
>- [OPSINFRA](https://laredoute.atlassian.net/jira/software/c/projects/OPSINFRA/boards/653)
>- [OPSPROD](https://laredoute.atlassian.net/jira/software/c/projects/OPSPROD/boards/491)
>- [OPST](https://laredoute.atlassian.net/jira/servicedesk/projects/OPST/issues)
>- [NET](https://laredoute.atlassian.net/jira/software/c/projects/NET/boards/249)

Here are our sharepoint portals:
>- [ITTeam](https://newr.sharepoint.com/sites/ITTeam)
>- [OPS Procedures](https://newr.sharepoint.com/sites/rfiles/RedoutePT/IS/ITSQD/gateway/Procedures/Forms/AllItems.aspx?OR=Teams-HL&CT=1710411806526&clickparams=eyJBcHBOYW1lIjoiVGVhbXMtRGVza3RvcCIsIkFwcFZlcnNpb24iOiI0OS8yNDAyMDIwNTUxNiIsIkhhc0ZlZGVyYXRlZFVzZXIiOmZhbHNlfQ%3D%3D)
>- [OPS Portugal Documentation](https://newr.sharepoint.com/sites/rfiles/RedoutePT/IS/ITSQD/gateway/Internal%20Documents/Forms/AllItems.aspx?viewpath=%2Fsites%2Frfiles%2FRedoutePT%2FIS%2FITSQD%2Fgateway%2FInternal%20Documents%2FForms%2FAllItems%2Easpx)

Other Important Links:
>- [Road to Cloud Migration](https://newr.sharepoint.com/sites/Road2CloudMigrations)
>- [Road to Cloud - Azure Foundation](https://newr.sharepoint.com/sites/Road2CloudMigrations/Documents%20partages/Forms/AllItems.aspx?viewid=bf524d21-99cb-4f86-b436-f31a23c843fb)
>- [Terraform Documentation](https://laredoute.atlassian.net/wiki/spaces/INF/pages/2943090842/Terraform+-+Service)
>- [IT Infrastructure Home (ARQ)](https://laredoute.atlassian.net/wiki/spaces/INF/overview?homepageId=1976631415)
>- [Service Catalog](https://laredoute.atlassian.net/wiki/spaces/INF/pages/2895315731/Service+Catalog)

